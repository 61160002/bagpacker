import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddChecklistForm extends StatefulWidget {
  String field;
  AddChecklistForm({Key? key, required this.field}) : super(key: key);

  @override
  _AddChecklistFormState createState() => _AddChecklistFormState(this.field);
}

class _AddChecklistFormState extends State<AddChecklistForm> {
  String field;
  _AddChecklistFormState(this.field);
  CollectionReference trips = FirebaseFirestore.instance.collection('trips');

  final _listController = TextEditingController();
  var newList = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(
              'BagPacker',
              style: TextStyle(
                  color: Color(0xff36383F), fontWeight: FontWeight.bold),
            ),
            backgroundColor: Color.fromRGBO(95, 182, 179, 1)),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(children: [
            Text('เพิ่มลิสต์',
                style: TextStyle(
                    color: Color(0xff36383F),
                    fontWeight: FontWeight.bold,
                    fontSize: 24)),
            Form(
              child: Container(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    TextFormField(
                      autofocus: true,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาใส่ข้อมูล';
                        }
                        return null;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      decoration: InputDecoration(labelText: field),
                      onChanged: (value) {
                        newList = value;
                      },
                    )
                  ],
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                Navigator.pop(context, newList);
              },
              child: Text('บันทึก',
                  style: TextStyle(
                      color: Color(0xff36383F),
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold)),
              style: ButtonStyle(
                  fixedSize: MaterialStateProperty.all(Size(90, 48)),
                  padding: MaterialStateProperty.all(EdgeInsets.all(20)),
                  backgroundColor: MaterialStateProperty.all(
                      Color.fromRGBO(177, 222, 224, 1))),
            )
          ]),
        ));
  }
}
