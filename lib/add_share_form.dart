import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddShareForm extends StatefulWidget {
  var tripId;
  AddShareForm({Key? key, required this.tripId}) : super(key: key);

  @override
  _AddShareFormState createState() => _AddShareFormState(this.tripId);
}

class _AddShareFormState extends State<AddShareForm> {
  var newShare = '';
  var tripId;
  String tripName = '';
  var share = [];
  var detail_trip = [];
  String goDate = '';
  String backDate = '';
  var hotels = [];
  var hotelValues = [false, false];
  var cruises = [];
  var cruiseValues = [false, false];
  var houses = [];
  var houseValues = [false, false];
  var camps = [];
  var campValues = [false, false, false, false, false];
  var motors = [];
  var motorValues = [false, false];
  var trains = [];
  var trainValues = [false, false];
  var cars = [];
  var carValues = [false, false];
  var planes = [];
  var planeValues = [false, false];
  var buss = [];
  var busValues = [false, false];
  var necessities = [];
  var necessitieValues = [false, false, false, false];
  var works = [];
  var workValues = [false, false];
  var clothes = [];
  var clotheValues = [false, false, false, false, false, false, false, false];
  var swims = [];
  var swimValues = [false, false, false];
  var elecs = [];
  var elecValues = [false, false, false];
  var treks = [];
  var trekValues = [false, false, false, false];
  var toilets = [];
  var toiletValues = [false, false, false, false, false, false, false, false];
  var beachs = [];
  var beachValues = [false, false, false];
  var todolist = [];
  var todolistValues = [false, false, false, false];
  _AddShareFormState(this.tripId);
  CollectionReference trips = FirebaseFirestore.instance.collection('trips');

  Future<void> updateShare() {
    return trips
        .doc(tripId)
        .update({'share': this.share})
        .then((value) => print('Share updated'))
        .catchError((error) => print("Failed to add user: $error"));
  }

  void initState() {
    super.initState();
    if (this.tripId.isNotEmpty) {
      trips.doc(this.tripId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          setState(() {
            tripName = data['name_trip'];
            share = data['share'];
            detail_trip = data['detail_trip'];
            goDate = data['goDate'];
            backDate = data['backDate'];
          });
        }
      });
    }
  }

  Future<void> addChecklist() async {
    if (detail_trip[0]) {
      setState(() {
        hotels = ['จองห้อง', 'หลักฐานการจอง'];
      });
    }
    if (detail_trip[1]) {
      setState(() {
        cruises = ['ยาแก้เมาเรือ', 'หลักฐานการจอง'];
      });
    }
    if (detail_trip[2]) {
      setState(() {
        houses = ['จองที่พัก', 'หลักฐานการจอง'];
      });
    }
    if (detail_trip[3]) {
      setState(() {
        camps = [
          'เต๊นท์',
          'ไฟฉาย',
          'ไฟแช็ก',
          'เครื่องครัวสนาม',
          'อุปกรณ์พยาบาล'
        ];
      });
    }
    if (detail_trip[4]) {
      setState(() {
        motors = ['เติมน้ำมัน', 'ใบขับขี่'];
      });
    }
    if (detail_trip[5]) {
      setState(() {
        trains = ['ตั๋วรถไฟ', 'ทิชชู่'];
      });
    }
    if (detail_trip[6]) {
      setState(() {
        cars = ['ยาแก้เมารถ', 'ใบขับขี่'];
      });
    }
    if (detail_trip[7]) {
      setState(() {
        planes = ['ตั๋วเครื่องบิน', 'พาสปอร์ต'];
      });
    }
    if (detail_trip[8]) {
      setState(() {
        buss = ['ตั๋วรถ', 'ยาแก้เมารถ'];
      });
    }
    if (detail_trip[9]) {
      setState(() {
        necessities = ['กระเป๋าตังค์', 'กุญแจบ้าน', 'ยาประจำตัว', 'อาหารเสริม'];
      });
    }
    if (detail_trip[10]) {
      setState(() {
        works = ['แล็ปท็อปและอุปกรณ์', 'นามบัตร'];
      });
    }
    if (detail_trip[11]) {
      setState(() {
        clothes = [
          'เสื้อ',
          'กางเกงขาสั้น',
          'กางเกงขายาว',
          'กระโปรง',
          'เดรส',
          'กางเกงใน',
          'ยกทรง',
          'ชุดนอน'
        ];
      });
    }
    if (detail_trip[12]) {
      setState(() {
        swims = ['ชุดว่ายน้ำ', 'หมวกว่ายน้ำ', 'แว่นตาว่ายน้ำ'];
      });
    }
    if (detail_trip[13]) {
      setState(() {
        elecs = ['สายชาร์จมือถือ', 'หูฟัง', 'พาวเวอร์แบงค์'];
      });
    }
    if (detail_trip[14]) {
      setState(() {
        treks = ['อุปกรณ์พยาบาล', 'ขวดน้ำ', 'รองเท้าเดินป่า', 'สเปรย์กันแมลง'];
      });
    }
    if (detail_trip[15]) {
      setState(() {
        toilets = [
          'สบู่',
          'ยาสระผม',
          'ครีมนวด',
          'ยาสีฟัน',
          'แปรงสีฟัน',
          'ผ้าเช็ดตัว',
          'โฟมล้างหน้า',
          'คอนแทคเลนส์'
        ];
      });
    }
    if (detail_trip[16]) {
      setState(() {
        beachs = ['ครีมกันแดด', 'ชุดว่ายน้ำ', 'แว่นกันแดด'];
      });
    }
    if (detail_trip[17]) {
      setState(() {
        todolist = [
          'ทิ้งขยะ',
          'ปิดหน้าต่าง',
          'ชาร์จแบตอุปกรณ์ต่างๆ',
          'รถน้ำต้นไม้'
        ];
      });
    }
  }

  Future<void> addTrip() {
    return trips
        .add({
          'userEmail': this.newShare,
          'share': this.share,
          'name_trip': this.tripName,
          'goDate': this.goDate,
          'backDate': this.backDate,
          'detail_trip': this.detail_trip,
          'hotels': this.hotels,
          'hotelValues': this.hotelValues,
          'cruises': this.cruises,
          'houses': this.houses,
          'camps': this.camps,
          'cruiseValues': this.cruiseValues,
          'houseValues': this.houseValues,
          'campValues': this.campValues,
          'motors': this.motors,
          'trains': this.trains,
          'cars': this.cars,
          'planes': this.planes,
          'buss': this.buss,
          'motorValues': this.motorValues,
          'trainValues': this.trainValues,
          'carValues': this.carValues,
          'planeValues': this.planeValues,
          'busValues': this.busValues,
          'necessities': this.necessities,
          'works': this.works,
          'clothes': this.clothes,
          'swims': this.swims,
          'elecs': this.elecs,
          'treks': this.treks,
          'toilets': this.toilets,
          'beachs': this.beachs,
          'todolist': this.todolist,
          'necessitieValues': this.necessitieValues,
          'workValues': this.workValues,
          'clotheValues': this.clotheValues,
          'swimValues': this.swimValues,
          'elecValues': this.elecValues,
          'trekValues': this.trekValues,
          'toiletValues': this.toiletValues,
          'beachValues': this.beachValues,
          'todolistValues': this.todolistValues
        })
        .then((value) => print('Trip added'))
        .catchError((error) => print("Failed to add user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(
              'BagPacker',
              style: TextStyle(
                  color: Color(0xff36383F), fontWeight: FontWeight.bold),
            ),
            backgroundColor: Color.fromRGBO(95, 182, 179, 1)),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(children: [
            Text('เพิ่มเพื่อน',
                style: TextStyle(
                    color: Color(0xff36383F),
                    fontWeight: FontWeight.bold,
                    fontSize: 24)),
            Form(
              child: Container(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    TextFormField(
                      autofocus: true,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาใส่ข้อมูล';
                        }
                        return null;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      decoration: InputDecoration(labelText: 'User Email'),
                      onChanged: (value) {
                        newShare = value;
                      },
                    )
                  ],
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                share.add(newShare);
                updateShare();
                addChecklist();
                addTrip();
                Navigator.pop(context, newShare);
              },
              child: Text('บันทึก',
                  style: TextStyle(
                      color: Color(0xff36383F),
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold)),
              style: ButtonStyle(
                  fixedSize: MaterialStateProperty.all(Size(90, 48)),
                  padding: MaterialStateProperty.all(EdgeInsets.all(20)),
                  backgroundColor: MaterialStateProperty.all(
                      Color.fromRGBO(177, 222, 224, 1))),
            )
          ]),
        ));
  }
}
