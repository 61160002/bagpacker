import 'package:bag_packer_project/checklist_bag_widget.dart';
import 'package:bag_packer_project/main.dart';
import 'package:bag_packer_project/share_widget.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AllTrips extends StatefulWidget {
  var userId;
  AllTrips({Key? key, required this.userId}) : super(key: key);

  @override
  _AllTripsState createState() => _AllTripsState(this.userId);
}

class _AllTripsState extends State<AllTrips> {
  String userId;
  _AllTripsState(this.userId);
  late Stream<QuerySnapshot> _tripsStream;
  CollectionReference trips = FirebaseFirestore.instance.collection('trips');

  @override
  void initState() {
    super.initState();
    _tripsStream = FirebaseFirestore.instance
        .collection('trips')
        .where('userId', isEqualTo: userId)
        .snapshots();
  }

  Future<void> deleteTrip(tripId) {
    return trips
        .doc(tripId)
        .delete()
        .then((value) => print("Trip Deleted"))
        .catchError((error) => print("Failed to delete user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        StreamBuilder<QuerySnapshot>(
          stream: _tripsStream,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Something went wrong');
            }

            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text("Loading");
            }
            return Column(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                return GestureDetector(
                    onTap: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ChecklistBagWidget(tripId: document.id)),
                      );
                    },
                    child: new Container(
                        margin: const EdgeInsets.all(10.0),
                        width: 400,
                        height: 150,
                        color: Color(0xff83CBC9),
                        padding: const EdgeInsets.all(14.0),
                        child: Column(
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                data['name_trip'],
                                style: TextStyle(
                                    fontSize: 24,
                                    color: Color(0xff36383F),
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                data['goDate'] + ' - ' + data['backDate'],
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Color(0xff36383F),
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Padding(padding: const EdgeInsets.all(14.0)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Text(
                                    'ขอให้เดินทางโดยสวัสดิภาพ :)',
                                    style: TextStyle(
                                        fontSize: 18, color: Color(0xff2D7F7F)),
                                  ),
                                ),
                                Row(
                                  children: [
                                    IconButton(
                                      icon: Icon(Icons.share,
                                          color: Color(0xff36383F)),
                                      iconSize: 24,
                                      onPressed: () async {
                                        await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ShareWidget(
                                                    tripId: document.id,
                                                  )),
                                        );
                                      },
                                    ),
                                    IconButton(
                                      icon: Icon(Icons.delete,
                                          color: Color(0xff36383F)),
                                      iconSize: 24,
                                      onPressed: () async {
                                        await deleteTrip(document.id);
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        )));
              }).toList(),
            );
          },
        ),
      ],
    );
  }
}
