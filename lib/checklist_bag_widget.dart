import 'package:bag_packer_project/add_checklist_form.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'main.dart';

class ChecklistBagWidget extends StatefulWidget {
  var tripId;
  ChecklistBagWidget({Key? key, required this.tripId}) : super(key: key);

  @override
  _ChecklistBagWidgetState createState() =>
      _ChecklistBagWidgetState(this.tripId);
}

class _ChecklistBagWidgetState extends State<ChecklistBagWidget> {
  var tripId;
  _ChecklistBagWidgetState(this.tripId);
  CollectionReference trips = FirebaseFirestore.instance.collection('trips');

  String tripName = '';
  var detail_trip = [];
  var hotels = [];
  var hotelValues = [];
  var cruises = [];
  var cruiseValues = [];
  var houses = [];
  var houseValues = [];
  var camps = [];
  var campValues = [];
  var motors = [];
  var motorValues = [];
  var trains = [];
  var trainValues = [];
  var cars = [];
  var carValues = [];
  var planes = [];
  var planeValues = [];
  var buss = [];
  var busValues = [];
  var necessities = [];
  var necessitieValues = [];
  var works = [];
  var workValues = [];
  var clothes = [];
  var clotheValues = [];
  var swims = [];
  var swimValues = [];
  var elecs = [];
  var elecValues = [];
  var treks = [];
  var trekValues = [];
  var toilets = [];
  var toiletValues = [];
  var beachs = [];
  var beachValues = [];
  var todolist = [];
  var todolistValues = [];

  @override
  void initState() {
    super.initState();
    if (this.tripId.isNotEmpty) {
      trips.doc(this.tripId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          setState(() {
            tripName = data['name_trip'];
            this.detail_trip = data['detail_trip'];
            this.hotels = data['hotels'];
            this.hotelValues = data['hotelValues'];
            this.cruises = data['cruises'];
            this.cruiseValues = data['cruiseValues'];
            this.houses = data['houses'];
            this.houseValues = data['houseValues'];
            this.camps = data['camps'];
            this.campValues = data['campValues'];
            this.motors = data['motors'];
            this.motorValues = data['motorValues'];
            this.trains = data['trains'];
            this.trainValues = data['trainValues'];
            this.cars = data['cars'];
            this.carValues = data['carValues'];
            this.planes = data['planes'];
            this.planeValues = data['planeValues'];
            this.buss = data['buss'];
            this.busValues = data['busValues'];
            this.necessities = data['necessities'];
            this.necessitieValues = data['necessitieValues'];
            this.works = data['works'];
            this.workValues = data['workValues'];
            this.clothes = data['clothes'];
            this.clotheValues = data['clotheValues'];
            this.swims = data['swims'];
            this.swimValues = data['swimValues'];
            this.elecs = data['elecs'];
            this.elecValues = data['elecValues'];
            this.treks = data['treks'];
            this.trekValues = data['trekValues'];
            this.toilets = data['toilets'];
            this.toiletValues = data['toiletValues'];
            this.beachs = data['beachs'];
            this.beachValues = data['beachValues'];
            this.todolist = data['todolist'];
            this.todolistValues = data['todolistValues'];
          });
        }
      });
    }
  }

  Future<void> updateTrip() {
    return trips
        .doc(tripId)
        .update({
          'hotels': this.hotels,
          'hotelValues': this.hotelValues,
          'cruises': this.cruises,
          'houses': this.houses,
          'camps': this.camps,
          'cruiseValues': this.cruiseValues,
          'houseValues': this.houseValues,
          'campValues': this.campValues,
          'motors': this.motors,
          'trains': this.trains,
          'cars': this.cars,
          'planes': this.planes,
          'buss': this.buss,
          'motorValues': this.motorValues,
          'trainValues': this.trainValues,
          'carValues': this.carValues,
          'planeValues': this.planeValues,
          'busValues': this.busValues,
          'necessities': this.necessities,
          'works': this.works,
          'clothes': this.clothes,
          'swims': this.swims,
          'elecs': this.elecs,
          'treks': this.treks,
          'toilets': this.toilets,
          'beachs': this.beachs,
          'todolist': this.todolist,
          'necessitieValues': this.necessitieValues,
          'workValues': this.workValues,
          'clotheValues': this.clotheValues,
          'swimValues': this.swimValues,
          'elecValues': this.elecValues,
          'trekValues': this.trekValues,
          'toiletValues': this.toiletValues,
          'beachValues': this.beachValues,
          'todolistValues': this.todolistValues,
        })
        .then((value) => print('Trip updated'))
        .catchError((error) => print("Failed to add user: $error"));
  }

  // void DeleteList(List<dynamic> name, List<dynamic> value, int n, int i) {
  //     name.removeAt(i);
  //     value.removeAt(i);
  //     print(name+value);
  //     setState(() {
  //       n = name.length;
  //     });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(
              'BagPacker',
              style: TextStyle(
                  color: Color(0xff36383F), fontWeight: FontWeight.bold),
            ),
            backgroundColor: Color.fromRGBO(95, 182, 179, 1)),
        body: ListView(
          children: [
            Container(
              child: Column(children: [
                Container(
                  color: Color.fromRGBO(177, 222, 224, 1),
                  height: 60,
                  width: 1600,
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Center(
                    child: Text(tripName,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                        )),
                  ),
                ),
              ]),
            ),
            Container(
                child: detail_trip[17] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('รายการที่ต้องทำ',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < todolist.length; i++)
                            CheckboxListTile(
                                value: todolistValues[i],
                                title: Text(todolist[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      todolist.removeAt(i);
                                      todolistValues.removeAt(i);
                                    });
                                    // DeleteList(todolist, todolistValues, nTodolist, i);
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    todolistValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการที่ต้องทำ')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    todolist.add(newList);
                                    todolistValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[0] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('โรงแรม',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < hotels.length; i++)
                            CheckboxListTile(
                                value: hotelValues[i],
                                title: Text(hotels[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      hotels.removeAt(i);
                                      hotelValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    hotelValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการโรงแรม')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    hotels.add(newList);
                                    hotelValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[1] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('เรือสำราญ',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < cruises.length; i++)
                            CheckboxListTile(
                                value: cruiseValues[i],
                                title: Text(cruises[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      cruises.removeAt(i);
                                      cruiseValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    cruiseValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการเรือสำราญ')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    cruises.add(newList);
                                    cruiseValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[2] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('บ้านพัก',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < houses.length; i++)
                            CheckboxListTile(
                                value: houseValues[i],
                                title: Text(houses[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      houses.removeAt(i);
                                      houseValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    houseValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการบ้านพัก')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    houses.add(newList);
                                    houseValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[3] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('แคมป์/เต๊นท์',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < camps.length; i++)
                            CheckboxListTile(
                                value: campValues[i],
                                title: Text(camps[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      camps.removeAt(i);
                                      campValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    campValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการแคมป์')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    camps.add(newList);
                                    campValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[4] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('รถจักรยานยนต์',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < motors.length; i++)
                            CheckboxListTile(
                                value: motorValues[i],
                                title: Text(motors[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      motors.removeAt(i);
                                      motorValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    motorValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการรถจักรยานยนต์')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    motors.add(newList);
                                    motorValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[5] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('รถไฟ',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < trains.length; i++)
                            CheckboxListTile(
                                value: trainValues[i],
                                title: Text(trains[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      trains.removeAt(i);
                                      trainValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    trainValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการรถไฟ')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    trains.add(newList);
                                    trainValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[6] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('รถยนต์',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < cars.length; i++)
                            CheckboxListTile(
                                value: carValues[i],
                                title: Text(cars[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      cars.removeAt(i);
                                      carValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    carValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการรถยนต์')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    cars.add(newList);
                                    carValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[7] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('เครื่องบิน',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < planes.length; i++)
                            CheckboxListTile(
                                value: planeValues[i],
                                title: Text(planes[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      planes.removeAt(i);
                                      planeValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    planeValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการเครื่องบิน')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    planes.add(newList);
                                    planeValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[8] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('รถโดยสาร',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < buss.length; i++)
                            CheckboxListTile(
                                value: busValues[i],
                                title: Text(buss[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      buss.removeAt(i);
                                      busValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    busValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการรถโดยสาร')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    buss.add(newList);
                                    busValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[9] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('ของใช้จำเป็น',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < necessities.length; i++)
                            CheckboxListTile(
                                value: necessitieValues[i],
                                title: Text(necessities[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      necessities.removeAt(i);
                                      necessitieValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    necessitieValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการของใช้จำเป็น')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    necessities.add(newList);
                                    necessitieValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[10] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('อุปกรณ์ทำงาน',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < works.length; i++)
                            CheckboxListTile(
                                value: workValues[i],
                                title: Text(works[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      works.removeAt(i);
                                      workValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    workValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการอุปกรณ์ทำงาน')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    works.add(newList);
                                    workValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[11] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('เสื้อผ้า',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < clothes.length; i++)
                            CheckboxListTile(
                                value: clotheValues[i],
                                title: Text(clothes[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      clothes.removeAt(i);
                                      clotheValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    clotheValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการเสื้อผ้า')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    clothes.add(newList);
                                    clotheValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[12] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('สระว่ายน้ำ',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < swims.length; i++)
                            CheckboxListTile(
                                value: swimValues[i],
                                title: Text(swims[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      swims.removeAt(i);
                                      swimValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    swimValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการสระว่ายน้ำ')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    swims.add(newList);
                                    swimValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[13] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('อิเล็กทรอนิกส์',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < elecs.length; i++)
                            CheckboxListTile(
                                value: elecValues[i],
                                title: Text(elecs[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      elecs.removeAt(i);
                                      elecValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    elecValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการอิเล็กทรอนิกส์')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    elecs.add(newList);
                                    elecValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[14] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('เดินป่า',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < treks.length; i++)
                            CheckboxListTile(
                                value: trekValues[i],
                                title: Text(treks[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      treks.removeAt(i);
                                      trekValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    trekValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการเดินป่า')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    treks.add(newList);
                                    trekValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[15] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('อุปกรณ์อาบน้ำ',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < toilets.length; i++)
                            CheckboxListTile(
                                value: toiletValues[i],
                                title: Text(toilets[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      toilets.removeAt(i);
                                      toiletValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    toiletValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการอุปกรณ์อาบน้ำ')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    toilets.add(newList);
                                    toiletValues.add(false);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                child: detail_trip[16] == true
                    ? new Column(
                        children: [
                          Container(
                            color: Color.fromRGBO(172, 203, 203, 1),
                            height: 40,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Text('ชายหาด',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                )),
                          ),
                          for (int i = 0; i < beachs.length; i++)
                            CheckboxListTile(
                                value: beachValues[i],
                                title: Text(beachs[i]),
                                secondary: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Color(0xff36383F)),
                                  iconSize: 18,
                                  onPressed: () {
                                    setState(() {
                                      beachs.removeAt(i);
                                      beachValues.removeAt(i);
                                    });
                                  },
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (newValue) {
                                  setState(() {
                                    beachValues[i] = newValue!;
                                  });
                                }),
                          Container(
                            height: 50,
                            width: 1600,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: IconButton(
                              icon: Icon(Icons.add, color: Color(0xff36383F)),
                              iconSize: 18,
                              onPressed: () async {
                                var newList = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddChecklistForm(
                                          field: 'รายการชายหาด')),
                                );
                                if (newList != null) {
                                  setState(() {
                                    beachs.add(newList);
                                    beachValues.add(false);
                                  });
                                }
                              }
                            ),
                          ),
                        ],
                      )
                    : null),
            Container(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        updateTrip();
                        await Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MyApp()),
                        );
                      },
                      child: Text('เสร็จสิ้น',
                          style: TextStyle(
                              color: Color(0xff36383F),
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold)),
                      style: ButtonStyle(
                          fixedSize: MaterialStateProperty.all(Size(90, 48)),
                          padding:
                              MaterialStateProperty.all(EdgeInsets.all(20)),
                          backgroundColor: MaterialStateProperty.all(
                              Color.fromRGBO(177, 222, 224, 1))),
                    )
                  ],
                ))
          ],
        ));
  }
}