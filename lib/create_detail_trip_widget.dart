import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'main.dart';

class CreateDetailTripWidget extends StatefulWidget {
  var userId;
  String userEmail;
  String tripName;
  String goDateStr;
  String backDateStr;
  int numDays;
  CreateDetailTripWidget({
    Key? key,
    required this.userId,
    required this.userEmail,
    required this.tripName,
    required this.goDateStr,
    required this.backDateStr,
    required this.numDays,
  }) : super(key: key);

  @override
  _CreateDetailTripWidgetState createState() => _CreateDetailTripWidgetState(
      this.userId,
      this.userEmail,
      this.tripName,
      this.numDays,
      this.backDateStr,
      this.goDateStr);
}

class _CreateDetailTripWidgetState extends State<CreateDetailTripWidget> {
  var detail_trip = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  var userId;
  String userEmail;
  String tripName;
  String goDateStr;
  String backDateStr;
  var share = [];
  int numDays;
  var hotels = [];
  var hotelValues = [false, false];
  var cruises = [];
  var cruiseValues = [false, false];
  var houses = [];
  var houseValues = [false, false];
  var camps = [];
  var campValues = [false, false, false, false, false];
  var motors = [];
  var motorValues = [false, false];
  var trains = [];
  var trainValues = [false, false];
  var cars = [];
  var carValues = [false, false];
  var planes = [];
  var planeValues = [false, false];
  var buss = [];
  var busValues = [false, false];
  var necessities = [];
  var necessitieValues = [false, false, false, false];
  var works = [];
  var workValues = [false, false];
  var clothes = [];
  var clotheValues = [false, false, false, false, false, false, false, false];
  var swims = [];
  var swimValues = [false, false, false];
  var elecs = [];
  var elecValues = [false, false, false];
  var treks = [];
  var trekValues = [false, false, false, false];
  var toilets = [];
  var toiletValues = [false, false, false, false, false, false, false, false];
  var beachs = [];
  var beachValues = [false, false, false];
  var todolist = [];
  var todolistValues = [false, false, false, false];
  CollectionReference trips = FirebaseFirestore.instance.collection('trips');
  CollectionReference checklists =
      FirebaseFirestore.instance.collection('checklists');
  _CreateDetailTripWidgetState(this.userId, this.userEmail, this.tripName, this.numDays,
      this.backDateStr, this.goDateStr);

  void initState() {
    super.initState();
  }

  Future<void> addTrip() {
    return trips
        .add({
          'userId': this.userId,
          'userEmail,': this.userEmail,
          'share': this.share,
          'name_trip': this.tripName,
          'goDate': this.goDateStr,
          'backDate': this.backDateStr,
          'numDays': this.numDays,
          'detail_trip': this.detail_trip,
          'hotels': this.hotels,
          'hotelValues': this.hotelValues,
          'cruises': this.cruises,
          'houses': this.houses,
          'camps': this.camps,
          'cruiseValues': this.cruiseValues,
          'houseValues': this.houseValues,
          'campValues': this.campValues,
          'motors': this.motors,
          'trains': this.trains,
          'cars': this.cars,
          'planes': this.planes,
          'buss': this.buss,
          'motorValues': this.motorValues,
          'trainValues': this.trainValues,
          'carValues': this.carValues,
          'planeValues': this.planeValues,
          'busValues': this.busValues,
          'necessities': this.necessities,
          'works': this.works,
          'clothes': this.clothes,
          'swims': this.swims,
          'elecs': this.elecs,
          'treks': this.treks,
          'toilets': this.toilets,
          'beachs': this.beachs,
          'todolist': this.todolist,
          'necessitieValues': this.necessitieValues,
          'workValues': this.workValues,
          'clotheValues': this.clotheValues,
          'swimValues': this.swimValues,
          'elecValues': this.elecValues,
          'trekValues': this.trekValues,
          'toiletValues': this.toiletValues,
          'beachValues': this.beachValues,
          'todolistValues': this.todolistValues
        })
        .then((value) => print('Trip added'))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> addChecklist() async {
    if (detail_trip[0]) {
      setState(() {
        hotels = ['จองห้อง', 'หลักฐานการจอง'];
      });
    }
    if (detail_trip[1]) {
      setState(() {
        cruises = ['ยาแก้เมาเรือ', 'หลักฐานการจอง'];
      });
    }
    if (detail_trip[2]) {
      setState(() {
        houses = ['จองที่พัก', 'หลักฐานการจอง'];
      });
    }
    if (detail_trip[3]) {
      setState(() {
        camps = [
          'เต๊นท์',
          'ไฟฉาย',
          'ไฟแช็ก',
          'เครื่องครัวสนาม',
          'อุปกรณ์พยาบาล'
        ];
      });
    }
    if (detail_trip[4]) {
      setState(() {
        motors = ['เติมน้ำมัน', 'ใบขับขี่'];
      });
    }
    if (detail_trip[5]) {
      setState(() {
        trains = ['ตั๋วรถไฟ', 'ทิชชู่'];
      });
    }
    if (detail_trip[6]) {
      setState(() {
        cars = ['ยาแก้เมารถ', 'ใบขับขี่'];
      });
    }
    if (detail_trip[7]) {
      setState(() {
        planes = ['ตั๋วเครื่องบิน', 'พาสปอร์ต'];
      });
    }
    if (detail_trip[8]) {
      setState(() {
        buss = ['ตั๋วรถ', 'ยาแก้เมารถ'];
      });
    }
    if (detail_trip[9]) {
      setState(() {
        necessities = ['กระเป๋าตังค์', 'กุญแจบ้าน', 'ยาประจำตัว', 'อาหารเสริม'];
      });
    }
    if (detail_trip[10]) {
      setState(() {
        works = ['แล็ปท็อปและอุปกรณ์', 'นามบัตร'];
      });
    }
    if (detail_trip[11]) {
      setState(() {
        clothes = [
          'เสื้อ',
          'กางเกงขาสั้น',
          'กางเกงขายาว',
          'กระโปรง',
          'เดรส',
          'กางเกงใน',
          'ยกทรง',
          'ชุดนอน'
        ];
      });
    }
    if (detail_trip[12]) {
      setState(() {
        swims = ['ชุดว่ายน้ำ', 'หมวกว่ายน้ำ', 'แว่นตาว่ายน้ำ'];
      });
    }
    if (detail_trip[13]) {
      setState(() {
        elecs = ['สายชาร์จมือถือ', 'หูฟัง', 'พาวเวอร์แบงค์'];
      });
    }
    if (detail_trip[14]) {
      setState(() {
        treks = ['อุปกรณ์พยาบาล', 'ขวดน้ำ', 'รองเท้าเดินป่า', 'สเปรย์กันแมลง'];
      });
    }
    if (detail_trip[15]) {
      setState(() {
        toilets = [
          'สบู่',
          'ยาสระผม',
          'ครีมนวด',
          'ยาสีฟัน',
          'แปรงสีฟัน',
          'ผ้าเช็ดตัว',
          'โฟมล้างหน้า',
          'คอนแทคเลนส์'
        ];
      });
    }
    if (detail_trip[16]) {
      setState(() {
        beachs = ['ครีมกันแดด', 'ชุดว่ายน้ำ', 'แว่นกันแดด'];
      });
    }
    if (detail_trip[17]) {
      setState(() {
        todolist = [
          'ทิ้งขยะ',
          'ปิดหน้าต่าง',
          'ชาร์จแบตอุปกรณ์ต่างๆ',
          'รถน้ำต้นไม้'
        ];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            'BagPacker',
            style: TextStyle(
                color: Color(0xff36383F), fontWeight: FontWeight.bold),
          ),
          backgroundColor: Color.fromRGBO(95, 182, 179, 1)),
      body: ListView(
        children: [
          Container(
            child: Column(children: [
              Container(
                color: Color.fromRGBO(177, 222, 224, 1),
                height: 60,
                width: 1600,
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Center(
                  child: Text(tripName,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                      )),
                ),
              ),
            ]),
          ),
          Container(
            child: Column(children: [
              Container(
                color: Color.fromRGBO(172, 203, 203, 1),
                height: 40,
                width: 1600,
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Text('ที่พัก',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    )),
              ),
            ]),
          ),
          Container(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[0] = !detail_trip[0]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[0]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('โรงแรม'),
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[1] = !detail_trip[1]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[1]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('เรือสำราญ'),
                  ],
                ),
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[2] = !detail_trip[2]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[2]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('บ้านพัก'),
                  ],
                ),
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[3] = !detail_trip[3]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[3]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('แคมป์/เต๊นท์'),
                  ],
                )
              ],
            ),
          ),
          Container(
            child: Column(children: [
              Container(
                color: Color.fromRGBO(172, 203, 203, 1),
                height: 40,
                width: 1600,
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Text('การเดินทาง',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    )),
              ),
            ]),
          ),
          Container(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[4] = !detail_trip[4]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[4]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('รถจักรยานยนต์'),
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[5] = !detail_trip[5]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[5]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('รถไฟ'),
                  ],
                ),
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[6] = !detail_trip[6]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[6]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('รถยนต์'),
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[7] = !detail_trip[7]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[7]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('เครื่องบิน'),
                  ],
                ),
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[8] = !detail_trip[8]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[8]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('รถโดยสาร'),
                  ],
                )
              ],
            ),
          ),
//---------กิจกรรม---------//
//---------กิจกรรม---------//
//---------กิจกรรม---------//
//---------กิจกรรม---------//
//---------กิจกรรม---------//
//---------กิจกรรม---------//
          Container(
            child: Column(children: [
              Container(
                color: Color.fromRGBO(172, 203, 203, 1),
                height: 40,
                width: 1600,
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Text('กิจกรรม / ของใช้ต่างๆ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    )),
              ),
            ]),
          ),
          Container(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[9] = !detail_trip[9]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[9]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('ของใช้จำเป็น'),
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[10] = !detail_trip[10]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[10]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('อุปกรณ์ทำงาน'),
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[11] = !detail_trip[11]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[11]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('เสื้อผ้า'),
                  ],
                ),
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[12] = !detail_trip[12]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[12]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('สระว่ายน้ำ'),
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[13] = !detail_trip[13]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[13]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('อิเล็กทรอนิกส์'),
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[14] = !detail_trip[14]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[14]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('เดินป่า'),
                  ],
                ),
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[15] = !detail_trip[15]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[15]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('อุปกรณ์อาบน้ำ'),
                    ElevatedButton(
                      onPressed: () async {
                        setState(() => detail_trip[16] = !detail_trip[16]);
                      },
                      child: Icon(Icons.add, color: Color(0xff36383F)),
                      style: ElevatedButton.styleFrom(
                        primary: detail_trip[16]
                            ? Colors.deepOrange[300]
                            : Color.fromRGBO(177, 222, 224, 1),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text('ชายหาด'),
                  ],
                )
              ],
            ),
          ),
          //---------อื่นๆ---------//
//---------อื่นๆ---------//
//---------อื่นๆ---------//
//---------อื่นๆ---------//
//---------อื่นๆ---------//
//---------อื่นๆ---------//
          Container(
            child: Column(children: [
              Container(
                color: Color.fromRGBO(172, 203, 203, 1),
                height: 40,
                width: 1600,
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Text('อื่นๆ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    )),
              ),
              Container(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                        ),
                        Column(children: [
                          ElevatedButton(
                            onPressed: () async {
                              setState(
                                  () => detail_trip[17] = !detail_trip[17]);
                            },
                            child: Icon(Icons.add, color: Color(0xff36383F)),
                            style: ElevatedButton.styleFrom(
                              primary: detail_trip[17]
                                  ? Colors.deepOrange[300]
                                  : Color.fromRGBO(177, 222, 224, 1),
                              padding: EdgeInsets.all(25),
                              shape: CircleBorder(),
                            ),
                          ),
                          Text('รายการที่ต้องทำ'),
                        ])
                      ]))
            ]),
          ),
          Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  ElevatedButton(
                    onPressed: () async {
                      addChecklist();
                      addTrip();
                      await Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => MyApp()),
                      );
                    },
                    child: Text('เสร็จสิ้น',
                        style: TextStyle(
                            color: Color(0xff36383F),
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold)),
                    style: ButtonStyle(
                        fixedSize: MaterialStateProperty.all(Size(90, 48)),
                        padding: MaterialStateProperty.all(EdgeInsets.all(20)),
                        backgroundColor: MaterialStateProperty.all(
                            Color.fromRGBO(177, 222, 224, 1))),
                  )
                ],
              )),
        ],
      ),
    );
  }
}
