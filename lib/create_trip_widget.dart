import 'package:bag_packer_project/create_detail_trip_widget.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'main.dart';

class CreateTripWidget extends StatefulWidget {
  String userId;
  String userEmail;
  CreateTripWidget({Key? key, required this.userId, required this.userEmail}) : super(key: key);

  @override
  _CreateTripWidgetState createState() => _CreateTripWidgetState(this.userId, this.userEmail);
}

class _CreateTripWidgetState extends State<CreateTripWidget> {
  String userId;
  String userEmail;
  String tripId = 'erng';

  CollectionReference trips = FirebaseFirestore.instance.collection('trips');
  _CreateTripWidgetState(this.userId, this.userEmail);

  @override
  void initState() {
    super.initState();
    if (this.userId.isNotEmpty) {
      // trips.doc(this.userId).get().then((snapshot) {
      //   if (snapshot.exists) {
      //     var data = snapshot.data() as Map<String, dynamic>;
      //     fullName = data['full_name'];
      //     company = data['company'];
      //     age = data['age'].toString();
      //     _fullNameController.text = fullName;
      //     _companyController.text = company;
      //     _ageController.text = age;
      //   }
      // });
    }
  }

  final _formKey = GlobalKey<FormState>();
  String tripName = '';
  DateTime currentDate = DateTime.now();
  DateTime goDate = DateTime.now();
  String goDateStr = '';
  DateTime backDate = DateTime.now();
  String backDateStr = '';
  String countDay = '';
  bool dateCheck = false;
  int numDays = 0;

  final _nameController = TextEditingController();

  Future<void> _selectGoDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime.now().subtract(Duration(days: 0)),
        lastDate: DateTime(2050));
    if (pickedDate!.difference(currentDate).inDays >= 0) {
      setState(() {
        goDate = pickedDate;
        goDateStr = '${pickedDate.day}' +
            '/${pickedDate.month}' +
            '/${pickedDate.year}';
      });
      _countDay();
    }
  }

  Future<void> _selectBackDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime.now().subtract(Duration(days: 0)),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate.difference(goDate).inDays >= 0) {
      setState(() {
        backDate = pickedDate;
        backDateStr = '${pickedDate.day}' +
            '/${pickedDate.month}' +
            '/${pickedDate.year}';
      });
      _countDay();
    }
  }

  Future<void> _countDay() async {
    numDays = backDate.difference(goDate).inDays;
    int numDayPlus = numDays + 1;

    if (goDate.isAfter(backDate)) {
      setState(() {
        countDay = 'วันที่กลับไม่ถูกต้อง';
        dateCheck = false;
      });
    } else {
      setState(() {
        countDay = '$numDayPlus วัน $numDays คืน';
        dateCheck = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            'BagPacker',
            style: TextStyle(
                color: Color(0xff36383F), fontWeight: FontWeight.bold),
          ),
          backgroundColor: Color.fromRGBO(95, 182, 179, 1)),
      body: ListView(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  'ทริปใหม่',
                  style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
              Form(
                key: _formKey,
                child: Container(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      TextFormField(
                        autofocus: true,
                        controller: _nameController,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'กรุณาใส่ข้อมูล';
                          }
                          return null;
                        },
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        decoration: InputDecoration(labelText: 'ชื่อทริป'),
                        onChanged: (value) {
                          setState(() {
                            tripName = value;
                          });
                        },
                      )
                    ],
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.all(10)),
              Container(
                width: 400,
                height: 100,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'วันที่ไป: ' +
                              goDate.day.toString() +
                              '-' +
                              goDate.month.toString() +
                              '-' +
                              goDate.year.toString(),
                          style: TextStyle(fontSize: 16.0),
                        ),
                        ElevatedButton(
                          onPressed: () => _selectGoDate(context),
                          child: Icon(Icons.edit, color: Color(0xff36383F)),
                          style: ButtonStyle(
                              padding: MaterialStateProperty.all(
                                  EdgeInsets.fromLTRB(16, 12, 16, 12)),
                              backgroundColor: MaterialStateProperty.all(
                                  Color.fromRGBO(177, 222, 224, 1))),
                        ),
                      ],
                    ),
                    Padding(padding: const EdgeInsets.all(8)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'วันที่กลับ: ' +
                              backDate.day.toString() +
                              '-' +
                              backDate.month.toString() +
                              '-' +
                              backDate.year.toString(),
                          style: TextStyle(fontSize: 16.0),
                        ),
                        ElevatedButton(
                          onPressed: () => _selectBackDate(context),
                          child: Icon(Icons.edit, color: Color(0xff36383F)),
                          style: ButtonStyle(
                              padding: MaterialStateProperty.all(
                                  EdgeInsets.fromLTRB(16, 12, 16, 12)),
                              backgroundColor: MaterialStateProperty.all(
                                  Color.fromRGBO(177, 222, 224, 1))),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(tripName + ' ($countDay)',
                      style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold,
                          color: (dateCheck == false)
                              ? Colors.red.withOpacity(0.8)
                              : Color.fromRGBO(95, 182, 179, 1)))),
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      ElevatedButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate() &&
                              dateCheck == true) {
                            await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CreateDetailTripWidget(
                                    userId: userId,
                                    userEmail: userEmail,
                                      tripName: tripName,
                                      numDays: numDays,
                                      goDateStr: goDateStr,
                                      backDateStr: backDateStr)),
                            );
                          }
                        },
                        child: Text('สร้าง',
                            style: TextStyle(
                                color: Color(0xff36383F),
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold)),
                        style: ButtonStyle(
                            fixedSize: MaterialStateProperty.all(Size(90, 48)),
                            padding:
                                MaterialStateProperty.all(EdgeInsets.all(20)),
                            backgroundColor: MaterialStateProperty.all(
                                Color.fromRGBO(177, 222, 224, 1))),
                      )
                    ],
                  )),
            ],
          ),
        ],
      ),
    );
  }
}
