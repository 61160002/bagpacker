import 'dart:async';
import 'package:bag_packer_project/share_with_me.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'all_trips.dart';
import 'create_trip_widget.dart';

void main() {
  runApp(App());
}

class App extends StatefulWidget {
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Text('Error..');
        } else if (snapshot.connectionState == ConnectionState.done) {
          return MyApp();
        }
        return Text('Loading..');
      },
    );
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  late StreamSubscription<User?> _sub;

  @override
  void initState() {
    super.initState();
    _sub = FirebaseAuth.instance.authStateChanges().listen((event) {
      print(event.toString());
      _navigatorKey.currentState!.pushReplacementNamed(
        event != null ? 'home' : 'login',
        // event != null ? 'home' : 'home',
      );
    });
  }

  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BagPacker',
      navigatorKey: _navigatorKey,
      initialRoute:
          FirebaseAuth.instance.currentUser == null ? 'login' : 'home',
          // FirebaseAuth.instance.currentUser == null ? 'home' : 'home',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case 'home':
            return MaterialPageRoute(
              settings: settings,
              builder: (_) => BottomBar(),
            );
          case 'login':
            return MaterialPageRoute(
              settings: settings,
              builder: (_) => LoginPage(),
            );
          default:
            return MaterialPageRoute(
              settings: settings,
              builder: (_) => UnknownPage(),
            );
        }
      },
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Future<UserCredential> signInWithGoogle() async {
    // Create a new provider
    GoogleAuthProvider googleProvider = GoogleAuthProvider();

    googleProvider
        .addScope('https://www.googleapis.com/auth/contacts.readonly');
    googleProvider.setCustomParameters({'login_hint': 'user@example.com'});

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithPopup(googleProvider);

    // Or use signInWithRedirect
    // return await FirebaseAuth.instance.signInWithRedirect(googleProvider);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.all(16),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Bag',
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 80.0,
                      fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 170, 0),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Packer ',
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 80.0,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  ':)',
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 80.0,
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(95, 182, 179, 1)),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.all(30.0),
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(
                        labelText: 'EMAIL',
                        labelStyle: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.grey)),
                  ),
                  Padding(
                    padding: EdgeInsets.all(16.0),
                  ),
                  TextField(
                    decoration: InputDecoration(
                        labelText: 'PASSWORD',
                        labelStyle: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.grey)),
                    obscureText: true,
                  ),
                  Padding(
                    padding: EdgeInsets.all(16.0),
                  ),
                  Container(
                    alignment: Alignment(1, 0),
                    child: InkWell(
                      child: Text('Forgot Password',
                          style: TextStyle(
                              color: Color.fromRGBO(95, 182, 179, 1),
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(30),
                  ),
                  ElevatedButton(
                    onPressed: () {},
                    child: Text(
                      'LOGIN',
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromRGBO(95, 182, 179, 1),
                      fixedSize: const Size(450, 60),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      await signInWithGoogle();
                    },
                    child: Text(
                      'LOGIN WITH GOOGLE',
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      side: BorderSide(width: 1.0, color: Colors.black),
                      primary: Colors.white,
                      fixedSize: const Size(450, 60),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(16),
                  ),
                  Container(
                    child: InkWell(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('New to BagPacker ',
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                            )),
                        Text('Sign Up',
                            style: TextStyle(
                                color: Color.fromRGBO(95, 182, 179, 1),
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline)),
                      ],
                    )),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    ));
  }
}

class BottomBar extends StatefulWidget {
  BottomBar({Key? key}) : super(key: key);

  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int _selectedPage = 0;
  var _pageOptions;
  var userEmail;
  final FirebaseAuth auth = FirebaseAuth.instance;
  late User? user = auth.currentUser;

  @override
  void initState() {
    super.initState();
    if (user != null) {
      userEmail = user!.email;
    }
    _pageOptions = [MyHomePage(), ShareWithMeWidget(userEmail: userEmail)];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: _pageOptions[_selectedPage],
          bottomNavigationBar: BottomNavigationBar(
              backgroundColor: Color.fromRGBO(95, 182, 179, 1),
              selectedItemColor: Colors.black,
              unselectedItemColor: Colors.white,
              currentIndex: _selectedPage,
              onTap: (int index) {
                setState(() {
                  _selectedPage = index;
                });
              },
              items: [
                BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.share), label: 'Share with me'),
              ]),
        ));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // var userId = 'hHLKlK8vIUglc7XRt8Y2lUMCpzk2';
  var userId;
  var userEmail;
  final FirebaseAuth auth = FirebaseAuth.instance;
  late User user;

  @override
  void initState() {
    super.initState();
    User? user = auth.currentUser;
    if (user != null) {
      userId = user.uid;
      userEmail = user.email;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            'BagPacker',
            style: TextStyle(
                color: Color(0xff36383F), fontWeight: FontWeight.bold),
          ),
          backgroundColor: Color.fromRGBO(95, 182, 179, 1)),
      body: Center(
        child: ListView(
          padding: const EdgeInsets.all(16.0),
          children: [
            ElevatedButton(
              onPressed: () async {
                print(userId + 'eiei');
                await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CreateTripWidget(
                          userId: userId, userEmail: userEmail)),
                );
              },
              child: Icon(Icons.add, color: Color(0xff36383F)),
              style: ButtonStyle(
                  shape: MaterialStateProperty.all(CircleBorder()),
                  padding: MaterialStateProperty.all(EdgeInsets.all(25)),
                  backgroundColor: MaterialStateProperty.all(
                      Color.fromRGBO(177, 222, 224, 1))),
            ),
            Text(
              'เพิ่มทริปใหม่',
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Padding(padding: EdgeInsets.all(8.0)),
            AllTrips(userId: userId),
          ],
        ),
      ),
    );
  }
}

class UnknownPage extends StatefulWidget {
  const UnknownPage({Key? key}) : super(key: key);

  @override
  _UnknownPageState createState() => _UnknownPageState();
}

class _UnknownPageState extends State<UnknownPage> {
  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.red);
  }
}
