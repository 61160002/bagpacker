import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'add_share_form.dart';
import 'main.dart';

class ShareWidget extends StatefulWidget {
  var tripId;
  ShareWidget({Key? key, required this.tripId}) : super(key: key);

  @override
  _ShareWidgetState createState() => _ShareWidgetState(this.tripId);
}

class _ShareWidgetState extends State<ShareWidget> {
  var tripId;
  CollectionReference trips = FirebaseFirestore.instance.collection('trips');
  _ShareWidgetState(this.tripId);
  late Stream<QuerySnapshot> _tripsStream;

  String tripName = '';
  var share = [];
  var detail_trip = [];
  String goDate = '';
  String backDate = '';

  @override
  void initState() {
    super.initState();
    if (this.tripId.isNotEmpty) {
      trips.doc(this.tripId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          setState(() {
            tripName = data['name_trip'];
            share = data['share'];
            detail_trip = data['detail_trip'];
            goDate = data['goDate'];
            backDate = data['backDate'];
          });
        }
      });
    }
  }

  Future<void> updateShare() {
    return trips
        .doc(tripId)
        .update({'share': this.share})
        .then((value) => print('Share updated'))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> deleteTrip(tripId) {
    return trips
        .doc(tripId)
        .delete()
        .then((value) => print("Trip Deleted"))
        .catchError((error) => print("Failed to delete user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(
              'BagPacker',
              style: TextStyle(
                  color: Color(0xff36383F), fontWeight: FontWeight.bold),
            ),
            backgroundColor: Color.fromRGBO(95, 182, 179, 1)),
        body: ListView(
          children: [
            Container(
              child: Column(children: [
                Container(
                  color: Color.fromRGBO(177, 222, 224, 1),
                  height: 60,
                  width: 1600,
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Center(
                    child: Text(tripName,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                        )),
                  ),
                ),
                Padding(padding: EdgeInsets.all(16.0)),
                Text('แชร์กับเพื่อน',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    )),
                Padding(padding: EdgeInsets.all(16.0)),
                for (int i = 0; i < share.length; i++)
                  ListTile(
                    title: Text(share[i]),
                    trailing: Text('Shared',style: TextStyle(
                      color: Colors.grey,
                      fontSize: 14,
                    )),
                  ),
                Padding(padding: EdgeInsets.all(16.0)),
                Container(
                  height: 50,
                  width: 1600,
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: IconButton(
                    icon: Icon(Icons.add, color: Color(0xff36383F)),
                    iconSize: 18,
                    onPressed: () async {
                      var newShare = await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddShareForm(
                                  tripId: tripId,
                                )),
                      );
                      if (newShare != null) {
                        setState(() {
                          share.add(newShare);
                        });
                      }
                    },
                  ),
                ),
                Padding(padding: EdgeInsets.all(16.0)),
                ElevatedButton(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyApp()),
                    );
                  },
                  child: Text('บันทึก',
                      style: TextStyle(
                          color: Color(0xff36383F),
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold)),
                  style: ButtonStyle(
                      fixedSize: MaterialStateProperty.all(Size(90, 48)),
                      padding: MaterialStateProperty.all(EdgeInsets.all(20)),
                      backgroundColor: MaterialStateProperty.all(
                          Color.fromRGBO(177, 222, 224, 1))),
                )
              ]),
            ),
          ],
        ));
  }
}
